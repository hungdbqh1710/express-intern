const express = require('express');
const auth = express.Router();
const AuthMiddleware = require('../App/Middlewares/AuthMiddleware');
const AuthController = require('../App/Controllers/Http/AuthController');

auth.post('/register', (req, res, next) => AuthController.register(req, res, next));
auth.post('/login', (req, res, next) => AuthController.login(req, res, next));
auth.post('/logout', (req, res, next) => AuthMiddleware.auth(req, res, next));
auth.post('/logout', (req, res) => AuthController.logout(req, res));

module.exports = auth;

