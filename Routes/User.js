const express = require('express');
const AuthMiddleware = require('../App/Middlewares/AuthMiddleware');
const UserController = require('../App/Controllers/Http/UserController');
const user = express.Router();

user.get('/profile', (req, res, next) => AuthMiddleware.auth(req, res, next));
user.get('/profile', (req, res) => UserController.getProfile(req, res));
user.patch('/update-profile', (req, res, next) => AuthMiddleware.auth(req, res, next));
user.patch('/update-profile', (req, res) => UserController.updateProfile(req, res));

module.exports = user;
