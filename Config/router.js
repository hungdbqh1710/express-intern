const app = require('./app');
const apiPrefix = '/api/v1';
const userRouter = require('../Routes/User');
const authRouter = require('../Routes/Auth');
const teamRouter = require('../Routes/Team');
app.use(`${apiPrefix}/user`, userRouter);
app.use(`${apiPrefix}/team`, teamRouter);
app.use(`${apiPrefix}/auth`, authRouter);

module.exports = app;
