const express = require('express');
const dotenv = require('dotenv').config();
var jwt = require('jsonwebtoken');
global.Env = process.env;
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.listen(Env.PORT, ()=>{
    console.log(`Server is listening port ${Env.PORT}`);
})

module.exports = app;