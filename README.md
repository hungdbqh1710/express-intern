# API
# Login
method: POST
url:    /api/v1/auth/login
body:   {
            "username": "hungdb",
            "password": "123456"
        }

# Register
method: POST
url:    /api/v1/auth/register
body:   {
            "username": "hungdb",
            "name": "Hung Dang",
            "password": "123456"
        }

# Logout 
method: POST
url:    /api/v1/auth/logout
bearer token: xxxx

# Get profile current user
method: GET
url:    /api/v1/user/profile
bearer token: xxxx

# Update user's name
method: PATCH
url:    /api/v1/user/update-profile
bearer token: xxxx
body:   {
            "name": "Dang Ba Hung"
        }
