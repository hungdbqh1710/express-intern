// Update with your config settings.
const dotenv = require('dotenv').config();
const { NODE_ENV } = process.env; 

module.exports = {
    client: process.env.DB_CLIENT,
    connection: {
        host : process.env.DB_HOST,
        user : process.env.DB_USERNAME,
        password : process.env.DB_PASSWORD,
        database : process.env.DB_NAME
    },
    pool: { min: 0, max: 7 },
    migrations: {
        tableName: 'migrations'
    }
}

/* {
  client: 'mysql',
  connection: {
    host : '127.0.0.1',
    user : 'your_database_user',
    password : 'your_database_password',
    database : 'myapp_test'
  }
} */