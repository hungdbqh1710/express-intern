exports.up = function(knex) {
    return knex.schema.createTable('users', (table) => {
         table.increments('id');
         table.string('name', 255).notNullable();
         table.string('username', 255).unique().index().notNullable();
         table.string('password', 255).notNullable();
         table.timestamps(true, true);
      });
  };
  
  exports.down = function(knex) {
    return knex.schema
        .dropTable("users");
  };
