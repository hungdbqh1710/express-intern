
exports.up = function(knex) {
    return knex.schema.createTable('tokens', (table) => {
        table.increments('id');
        table.integer('user_id').unsigned().notNullable();
        table.string('token').notNullable();
        table.string('status').default('active');
        table.timestamps(true, true);
        table.foreign('user_id').references('id').inTable('users');
     });
};

exports.down = function(knex) {
    return knex.schema.dropTable("tokens");
};
