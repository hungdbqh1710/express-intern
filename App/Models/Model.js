// run the following command to install:
// npm install objection knex sqlite3

const { Model } = require('objection');
const Knex = require('knex');
const knexFile = require('../../knexfile');

// Initialize knex.
const knex = Knex({ ...knexFile });

// Give the knex instance to objection.
Model.knex(knex);

module.exports = Model;