class UserMiddleware{
    /**
     * Get list user
     * @param {*} req 
     * @param {*} res 
     * @param {*} next 
     */
    static getList(req, res, next) {
        res.status(200).send({
            'message': 'Get list users success'
        });
        next();
    }

    /**
     * Get detail by id
     * @param {*} req 
     * @param {*} res 
     * @param {*} next 
     */
    static getDetail(req, res, next) {
        const { id } = req.params;
        res.status(200).send({
            'message': `Get detail user ${id} success !`
        });
        next();
    }

    /**
     * Create User
     * @param {*} req 
     * @param {*} res 
     * @param {*} next 
     */
    static createUser(req, res, next) {
        const {name, age} = req.body;
        res.status(201).send({
            'message': `Created user ${name} success !`
        });
        next();
    }

    /**
     * Update user
     * @param {*} req 
     * @param {*} res 
     * @param {*} next 
     */
    static updateUser(req, res, next) {
        const {id} = req. params;
        const {name, age} = req.body;
        res.status(200).send({
            'message': `Updated user ${id} success !`
        });
        next();
    }

    /**
     * Delete User
     * @param {*} req 
     * @param {*} res 
     * @param {*} next 
     */
    static deleteUser(req, res, next) {
        const {id} = req.params;
        res.status(200).send({
            'message': `Deleted user ${id} success !`
        });
        next();
    }
}
module.exports = UserMiddleware;