const jwt = require('json-web-token');
const TokenModel = require('../Models/TokenModel');

class AuthMiddleware {
    constructor() { 
        this.tokenModel = TokenModel;
    }

    /**
     * Middleware check auth of user
     * @param {*} req 
     * @param {*} res 
     * @param {*} next 
     */
    async auth(req, res, next) {
        const header = req.headers['authorization'];
        const token = header.split(' ')[1];
        let check_token = await this.tokenModel.query().where({token, status: 'active'}).first();
        if(!check_token){
            return res.status(403).json({
                message: 'User not exist or this token has expired',
                data: null
            })
        }
        req.token = token;
        req.userId = check_token.user_id;
        next();
    }

    /**
     * 
     */
    noAuth({req, res, next}) {

    }
}

module.exports = new AuthMiddleware();
