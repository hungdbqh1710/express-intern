class PostMiddleware{
    /**
     * Get list posts
     * @param {*} req 
     * @param {*} res 
     * @param {*} next 
     */
    static getList(req, res, next) {
        res.status(200).send({
            'message': 'Get list posts success !'
        });
        next();
    }

    /**
     * Get post detail by id
     * @param {*} req 
     * @param {*} res 
     * @param {*} next 
     */
    static getDetail(req, res, next) {
        const { id } = req.params;
        res.status(200).send({
            'message': `Get detail post ${id} success !`
        });
        next();
    }

    /**
     * Create post
     * @param {*} req 
     * @param {*} res 
     * @param {*} next 
     */
    static createPost(req, res, next) {
        //console.log(req);
        //const {author, title} = req.body;
        res.status(201).send({
            'message': `Created post success !`
        });
        next();
    }

    /**
     * Update post
     * @param {*} req 
     * @param {*} res 
     * @param {*} next 
     */
    static updatePost(req, res, next) {
        const {id} = req. params;
        const {author, title} = req.body;
        res.status(200).send({
            'message': `Updated post ${id} success !`
        });
        next();
    }

    /**
     * Delete post
     * @param {*} req 
     * @param {*} res 
     * @param {*} next 
     */
    static deletePost(req, res, next) {
        const {id} = req.params;
        res.status(200).send({
            'message': `Deleted post ${id} success !`
        });
        next();
    }
}
module.exports = PostMiddleware;