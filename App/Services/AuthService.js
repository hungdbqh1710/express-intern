const UserModel = require('../Models/UserModel');
const TokenModel = require('../Models/TokenModel');
class AuthService {
    
    //Check to make sure header is not undefined, if so, return Forbidden (403)
    static checkToken (req, res, next) {
        const header = req.headers['authorization'];

        if(typeof header !== 'undefined') {
            const token = header.split(' ')[1];
            let check_token = await TokenModel.query().where({token}).first();
            if(!check_token){
                return false; 
            }
            req.token = token;
            next();
        } else {
            //If header is undefined return Forbidden (403)
            res.sendStatus(403)
        }
    }
}

module.exports = AuthService;
