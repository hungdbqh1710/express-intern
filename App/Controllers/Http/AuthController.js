const UserModel = require('../../Models/UserModel');
const TokenModel = require('../../Models/TokenModel');
const bcrypt = require('bcryptjs');
const passport = require('passport');
const dotenv = require('dotenv');
const jwt = require('json-web-token');
const {ENCODE_KEY} = process.env;

/**
* This middleware validates authenticate user by JWT header.
*/
exports.accessToken = passport.authenticate('jwt', { session: false });

class AuthController {
    constructor() {
        this.userModel = UserModel;
        this.tokenModel = TokenModel;
    }

    /**
     * Register function
     * @param {*} req 
     * @param {*} res 
     * @param {*} next 
     */
    async register(req, res, next) {
        const {body} = req;
        const { name, username, password } = body;
        const { USER_PASSWORD_SALT_ROUNDS: saltRounds = 10 } = process.env;
        const passwordHash = await bcrypt.hash(password, +saltRounds);

        if(!username || !password){
            return res.json({
                message: 'username_or_password_is_required',
                data: null
            })
        }
        const check_username = await this.userModel.query().where({username}).first(); 
        if(check_username){
            return res.json({
                message: 'Username is existed',
                data: null
            })
        }
        let user = await this.userModel.query().insert(
            { 
                name, 
                username,
                password: passwordHash
            }
        );
        let token = await jwt.encode(ENCODE_KEY, {
            id: user.id,
            timestamps: new Date()
        });

        if(!token.value){
            return res.json({
                message: token.error.message,
                data: null
            })
        }
        await this.tokenModel.query().insert({
            token: token.value,
            status: 'active',
            user_id: user.id
        }) 
        res.json({
            message: 'Register success !',
            data: token.value
        })
    }

    /**
     * Login function
     * @param {*} req 
     * @param {*} res 
     * @param {*} next 
     */
    async login(req, res, next) {
        const { username, password } = req.body;
        if(!username || !password){
            return res.json({
                message: 'username_or_password_is_required',
                data: null
            })
        }

        const user = await this.userModel.query().where({username}).first(); 
        if(!user){
            return res.json({
                message: 'Username is not existed',
                data: null
            })
        }

        const check_psw = await bcrypt.compare(password, user.password);
        if(!check_psw){
            return res.json({
                message: 'Your password entered is incorrect',
                data: null
            })
        }

        let token = await jwt.encode(ENCODE_KEY, {
            id: user.id,
            timestamps: new Date()
        });

        if(!token.value){
            return res.json({
                message: token.error.message,
                data: null
            })
        }
        await this.tokenModel.query().patch({token: token.value, status: 'active'}).where({user_id: user.id}).first();
        res.json({
            message: 'Login success',
            data: token.value
        })
    }

    /**
     * Logout Function
     * @param {*} req 
     * @param {*} res 
     */
    async logout(req, res) {
        const { userId } = req; 
        console.log(userId);
        await this.tokenModel.query().patch({status: 'inactive'}).where({user_id: userId}).first();
        res.json({
            message: 'Logout success',
            data: null
        })
    }
}

module.exports = new AuthController;

