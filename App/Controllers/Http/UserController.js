const UserModel = require('../../Models/UserModel');
const TokenModel = require('../../Models/TokenModel');

class UserController {
    constructor() {
        this.userModel = UserModel;
        this.tokenModel = TokenModel;
    }

    /**
     * Get profile current user
     * @param {*} req 
     * @param {*} res 
     * @param {*} next 
     */
    async getProfile(req, res) {
        const { userId, token } = req;
        const data = await this.userModel.query().findById(userId);
        res.status(200).json({
            message: 'Get profile success',
            data
        });
    }

    /**
     * Update profile user ( update user's name )
     * @param {*} req 
     * @param {*} res 
     */
    async updateProfile(req, res) {
        const { userId } = req;
        const { name } = req.body;
        if(!name) {
            return res.json({
                message: 'Name is required',
                data: null
            });
        }
        await this.userModel.query().patch({name}).where({id: userId});
        let data = await this.userModel.query().findById(userId);
        res.status(200).json({
            message: 'Update profile success',
            data
        })
    }
}

module.exports = new UserController;


